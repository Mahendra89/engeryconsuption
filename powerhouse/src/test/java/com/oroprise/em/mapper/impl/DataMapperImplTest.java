package com.oroprise.em.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.oroprise.em.domain.MeterDetailsJpa;
import com.oroprise.em.domain.ProfileDetailsJpa;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;

public class DataMapperImplTest {
	@InjectMocks
	private DataMapperImpl mapperImpl;

	@BeforeClass
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void mapProfileDtailToProfileDetailJpa(){
		List<ProfileDetailsJpa> result=mapperImpl.mapProfileDtailToProfileDetailJpa(getProfileDetailsMockData());
		Assert.assertEquals(result.size(), 1);
	}
	@Test
	public void mapProfileDtailJpaToProfileDetail(){
		List<ProfileDetails> result=mapperImpl.mapProfileDtailJpaToProfileDetail(getProfileDetailsJpaMockData());
		Assert.assertEquals(result.size(), 1);
	}
	@Test
	public void mapMeterDtailToProfileDetailJpa(){
		List<MeterDetailsJpa> result=mapperImpl.mapMeterDetailsToMeterDetailsJpa(getMeterDetailsMockData());
		Assert.assertEquals(result.size(), 1);
	}
	@Test
	public void mapMeterDtailToProfileDetail(){
		List<MeterDetails> result=mapperImpl.mapMeterDetailsJpaToMeterDetails(getMeterDetailsJpaMockData());
		Assert.assertEquals(result.size(), 1);
	}
	
	private List<ProfileDetails> getProfileDetailsMockData(){
		List<ProfileDetails> profileDetails=new ArrayList<>();
		ProfileDetails profileDetail = new ProfileDetails();
		profileDetail.setProfileName("A");
		profileDetail.setMonth("JAN");
		profileDetail.setFraction("0.2");
		profileDetails.add(profileDetail);
		return profileDetails;
	}
	
	private List<ProfileDetailsJpa> getProfileDetailsJpaMockData(){
		List<ProfileDetailsJpa> profileDetailJpas = new ArrayList<>();
		ProfileDetailsJpa profileDetailJpa = new ProfileDetailsJpa();
		profileDetailJpa.setProfileName("A");
		profileDetailJpa.setMonth("JAN");
		profileDetailJpa.setFraction(0.2f);
		profileDetailJpas.add(profileDetailJpa);
		return profileDetailJpas;
	}
	
	private List<MeterDetailsJpa> getMeterDetailsJpaMockData(){
		List<MeterDetailsJpa> meterDetailsJpas = new ArrayList<>();
		MeterDetailsJpa meterDetailsJpa = new MeterDetailsJpa();
		meterDetailsJpa.setMeterId("1");
		meterDetailsJpa.setMeterReading(10);
		meterDetailsJpa.setMonth("JAN");
		meterDetailsJpa.setProfileName("A");
		meterDetailsJpas.add(meterDetailsJpa);
		return meterDetailsJpas;
				
	}
	
	private List<MeterDetails> getMeterDetailsMockData(){
		List<MeterDetails> meterDetailList = new ArrayList<>();
		MeterDetails meterDetail = new MeterDetails();
		meterDetail.setMeterId("1");
		meterDetail.setProfileName("A");
		meterDetail.setMonth("JAN");
		meterDetail.setMeterReading("10");
		meterDetailList.add(meterDetail);
		return meterDetailList;
	}
}
