package com.oroprise.em.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.EnergyConsuptionService;

public class EnergyConsuptionControllerTest {
 private MockMvc mockMvc;
 @InjectMocks
 private EnergyConsuptionController ecController;
 
 @Mock
 private EnergyConsuptionService energyConsuptionService;
 @BeforeClass
 public void setUp(){
	 MockitoAnnotations.initMocks(this);
     mockMvc = MockMvcBuilders.standaloneSetup(ecController).build(); 
 }
 @Test
 public void getConsuptionByMeterIdAndMonth() throws Exception{
Mockito.when(energyConsuptionService.getConsuptionByMeterIdAndMont("1", "JAN")).thenReturn(1);
   mockMvc.perform(get("/em/consuption/1/JAN"))
       .andExpect(status().isOk()).andDo(new ResultHandler() {

           @Override
           public void handle(MvcResult mvcResult) throws Exception {

               int result = new Gson().fromJson(
                   mvcResult.getResponse().getContentAsString(),
                   Integer.class);

               Assert.assertEquals(result, 1);
           }
       });
 }
 @Test
 public void getProfileDetails() throws Exception {

     List<ProfileDetails> profileDetails = new ArrayList<>();
     ProfileDetails profileDetail = new ProfileDetails();
     profileDetails.add(profileDetail);
     Mockito.when(energyConsuptionService.getProfileDetails())
         .thenReturn(profileDetails);

     mockMvc.perform(get("/em/profileDetails"))
         .andExpect(status().isOk()).andDo(new ResultHandler() {

             @Override
             public void handle(MvcResult mvcResult) throws Exception {

                 List result = new Gson().fromJson(
                     mvcResult.getResponse().getContentAsString(),
                     List.class);

                 Assert.assertEquals(result.size(), 1);
             }
         });

 }
 
 @Test
 public void getMeterDetails() throws Exception {
	 
	 List<MeterDetails> meterDetails = new ArrayList<>();
	 MeterDetails meterDetail = new MeterDetails();
	 meterDetails.add(meterDetail);
	 Mockito.when(energyConsuptionService.getMeterDetails())
	 .thenReturn(meterDetails);
	 
	 mockMvc.perform(get("/em/meterDetails"))
	 .andExpect(status().isOk()).andDo(new ResultHandler() {
		 
		 @Override
		 public void handle(MvcResult mvcResult) throws Exception {
			 
			 List result = new Gson().fromJson(
					 mvcResult.getResponse().getContentAsString(),
					 List.class);
			 
			 Assert.assertEquals(result.size(), 1);
		 }
	 });
	 
 }
}
