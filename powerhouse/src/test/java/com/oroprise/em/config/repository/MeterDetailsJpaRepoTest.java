package com.oroprise.em.config.repository;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.Assert;

import com.oroprise.em.domain.MeterDetailsJpa;
import com.oroprise.em.jpa.repository.MeterDetailsJpaRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MeterDetailsJpaRepoTest {
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private MeterDetailsJpaRepository meteterDetailsJpaRepo;
	//@Test
	public void findConsuptionByMeterIdAndMonth(){
		entityManager.persist(getMeterDetailsJpaMockData());
		int result=meteterDetailsJpaRepo.findConsuptionByMeterIdAndMonth("1", "JAN");
		Assert.assertEquals(result, 10);
	}
	
	private List<MeterDetailsJpa> getMeterDetailsJpaMockData(){
		List<MeterDetailsJpa> meterDetailsJpas = new ArrayList<>();
		MeterDetailsJpa meterDetailsJpa = new MeterDetailsJpa();
		meterDetailsJpa.setMeterId("1");
		meterDetailsJpa.setMeterReading(10);
		meterDetailsJpa.setMonth("JAN");
		meterDetailsJpa.setProfileName("A");
		meterDetailsJpas.add(meterDetailsJpa);
		return meterDetailsJpas;
				
	}
}
