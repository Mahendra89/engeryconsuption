package com.oroprise.em.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.Test;

import com.oroprise.em.jpa.repository.MeterDetailsJpaRepository;
import com.oroprise.em.mapper.DataMapper;
import com.oroprise.em.modal.MeterDetails;

//@RunWith(SpringRunner.class)
//@DataJpaTest
public class MeterDetailServiceImplTest {
	// @Autowired
	// private TestEntityManager entityManager;
	@Mock
	private MeterDetailsJpaRepository meteterDetailsJpaRepo;

	/** The data mapper. */
	@Mock
	private DataMapper dataMapper;
	@InjectMocks
	private MeterDetailServiceImpl meterDetailService;

	//@Test
	public void saveMeterDetails() {
		meterDetailService.saveMeterDetails(getMeterDetailsMockData());

	}

	private List<MeterDetails> getMeterDetailsMockData() {
		List<MeterDetails> meterDetailList = new ArrayList<>();
		MeterDetails meterDetail = new MeterDetails();
		meterDetail.setMeterId("1");
		meterDetail.setProfileName("A");
		meterDetail.setMonth("JAN");
		meterDetail.setMeterReading("10");
		meterDetailList.add(meterDetail);
		return meterDetailList;
	}
}
