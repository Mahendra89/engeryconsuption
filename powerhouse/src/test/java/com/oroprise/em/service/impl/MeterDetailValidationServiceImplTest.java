package com.oroprise.em.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.MeterDetails;

@SpringBootTest
@DirtiesContext
public class MeterDetailValidationServiceImplTest extends AbstractTestNGSpringContextTests {
	@Autowired
	@InjectMocks
	private MeterDetailValidationServiceImpl meterDetailValidationServiceImpl;

	@Test
	public void validate() throws ValidationException {
		List<MeterDetails> meterDetails = getMockData();
		List<MeterDetails> result = meterDetailValidationServiceImpl.validate(meterDetails);
		Assert.assertEquals(result.size(), 1);
	}

	private List<MeterDetails> getMockData() {
		List<MeterDetails> meterDetails = new ArrayList<>();
		MeterDetails meterDetails1 = new MeterDetails();
		meterDetails1.setMeterId("01");
		meterDetails1.setMeterReading("40");
		meterDetails1.setMonth("JAN");
		meterDetails1.setProfileName("A");
		meterDetails.add(meterDetails1);
		MeterDetails meterDetails2 = new MeterDetails();
		meterDetails2.setMeterId("01");
		meterDetails2.setMeterReading("19");
		meterDetails2.setMonth("FEB");
		meterDetails2.setProfileName("A");
		meterDetails.add(meterDetails2);

		MeterDetails meterDetails3 = new MeterDetails();
		meterDetails3.setMeterId("02");
		meterDetails3.setMeterReading("39");
		meterDetails3.setMonth("FEB");
		meterDetails3.setProfileName("B");
		meterDetails.add(meterDetails3);
		return meterDetails;
	}

}
