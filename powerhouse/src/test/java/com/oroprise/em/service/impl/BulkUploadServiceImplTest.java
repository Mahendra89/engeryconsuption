package com.oroprise.em.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.mockito.InjectMocks;
import org.springframework.mock.web.MockMultipartFile;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.ProfileDetails;
public class BulkUploadServiceImplTest {
	@InjectMocks
	private FileUploadServiceImpl bulkUploadServ;
	
@Test(enabled=false)
public void processProfileDeatails() throws IOException, ValidationException{
	 File file = new File("powerhouse/profieData.csv");
     FileInputStream fis = new FileInputStream(file);
     MockMultipartFile f = new MockMultipartFile("file", file.getName(),
         "multipart/form-data", fis);
     List<ProfileDetails> result = bulkUploadServ.processProfileDetais(f);
     Assert.assertNotNull(result);
}
}
