package com.oroprise.em.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.ProfileDetails;

@SpringBootTest
@DirtiesContext
public class ProfileDetailValidationServiceImplTest extends AbstractTestNGSpringContextTests {
	@Autowired
	private ProfileDetailsValidationServiceImpl profileDetailValidationServiceImpl;

	@Test(expectedExceptions=ValidationException.class)
	public void validateNegative() throws ValidationException {
		List<ProfileDetails> profiles = getProfileMockData("0.2");
		List<ProfileDetails> result=profileDetailValidationServiceImpl.validate(profiles);
		Assert.assertNotNull(result);
	}
	@Test
	public void validatePositive() throws ValidationException {
		List<ProfileDetails> profiles = getProfileMockData("0.1");
		List<ProfileDetails> result=profileDetailValidationServiceImpl.validate(profiles);
		Assert.assertEquals(result.size(), 12);
	}

	private List<ProfileDetails> getProfileMockData(String fraction) {
		List<ProfileDetails> profiles = new ArrayList<>();

		ProfileDetails profile = new ProfileDetails();
		profile.setProfileName("A");
		profile.setFraction(fraction);
		profiles.add(profile);

		ProfileDetails profile1 = new ProfileDetails();
		profile1.setProfileName("A");
		profile1.setFraction(fraction);
		profiles.add(profile1);

		ProfileDetails profile2 = new ProfileDetails();
		profile2.setProfileName("A");
		profile2.setFraction(fraction);
		profiles.add(profile2);

		ProfileDetails profile3 = new ProfileDetails();
		profile3.setProfileName("A");
		profile3.setFraction(fraction);
		profiles.add(profile3);

		ProfileDetails profile4 = new ProfileDetails();
		profile4.setProfileName("A");
		profile4.setFraction(fraction);
		profiles.add(profile4);

		ProfileDetails profile5 = new ProfileDetails();
		profile5.setProfileName("A");
		profile5.setFraction(fraction);
		profiles.add(profile5);

		ProfileDetails profile6 = new ProfileDetails();
		profile6.setProfileName("A");
		profile6.setFraction(fraction);
		profiles.add(profile6);

		ProfileDetails profile7 = new ProfileDetails();
		profile7.setProfileName("A");
		profile7.setFraction(fraction);
		profiles.add(profile7);
		ProfileDetails profile8 = new ProfileDetails();
		profile8.setProfileName("A");
		profile8.setFraction(fraction);
		profiles.add(profile8);
		ProfileDetails profile9 = new ProfileDetails();
		profile9.setProfileName("A");
		profile9.setFraction(fraction);
		profiles.add(profile9);
		ProfileDetails profile10 = new ProfileDetails();
		profile10.setProfileName("A");
		profile10.setFraction(fraction);
		profiles.add(profile10);
		ProfileDetails profile11 = new ProfileDetails();
		profile11.setProfileName("A");
		profile11.setFraction(fraction);
		profiles.add(profile11);
		return profiles;
	}
}
