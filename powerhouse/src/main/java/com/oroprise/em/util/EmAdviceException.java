package com.oroprise.em.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.oroprise.em.exception.BusinessException;


/**
 * The Class EmAdviceException.
 */
@ControllerAdvice
public class EmAdviceException {
	
	 /** The Constant LOGGER. */
 	private static final Logger LOGGER =
		        LoggerFactory.getLogger(EmAdviceException.class);

		    /**
		     * Constructor.
		     */
		    public EmAdviceException() {
		        super();
		    }

		    /**
    		 * Handle 404 error.
    		 *
    		 * @param request the request
    		 * @param e the e
    		 * @return the response entity
    		 */
    		@ExceptionHandler(NoHandlerFoundException.class)
		    public ResponseEntity<ErrorResponse> handle404Error(HttpServletRequest request,
		                                                        Exception e) {

		        LOGGER.warn("404 - " + request.getRequestURI());
		        ErrorResponse response =
		            createErrorResponse("404", "No such resource path");
		        return new ResponseEntity<ErrorResponse>(response,
		            HttpStatus.NOT_FOUND);
		    }

		    /**
    		 * Handle 400 error.
    		 *
    		 * @param request the request
    		 * @param exception the exception
    		 * @return the response entity
    		 */
    		@ExceptionHandler({HttpMessageNotReadableException.class,
		        MethodArgumentNotValidException.class,
		        HttpRequestMethodNotSupportedException.class})
		    public ResponseEntity<ErrorResponse> handle400Error(HttpServletRequest request,
		                                                        Exception exception) {

		        LOGGER.warn("400 - " + request.getRequestURI() + " | "
		            + exception.getMessage());
		        ErrorResponse response =
		            createErrorResponse("400", "Bad request received");
		        return new ResponseEntity<ErrorResponse>(response,
		            HttpStatus.BAD_REQUEST);
		    }


		    /**
    		 * Handles the RunTime exceptions thrown from the Business flows and sends appropriate response
    		 * to the client.
    		 *
    		 * @param runtimeException the runtime exception
    		 * @return ResponseEntity<ErrorResponse>
    		 */
		    @ExceptionHandler(RuntimeException.class)
		    public ResponseEntity<ErrorResponse> handle500Error(RuntimeException runtimeException) {

		        LOGGER.error("Runtime exception caught", runtimeException);
		        ErrorResponse response = createErrorResponse("500",
		            "Request failed. Please contact your adminstrator");
		        return new ResponseEntity<ErrorResponse>(response,
		            HttpStatus.INTERNAL_SERVER_ERROR);
		    }



		    /**
    		 * Handles all Business Exceptions thrown from Business flows and sends appropriate response to
    		 * the client.
    		 *
    		 * @param businessException the business exception
    		 * @return ResponseEntity<ErrorResponse>
    		 */
		    @ExceptionHandler(BusinessException.class)
		    public ResponseEntity<ErrorResponse> handleBusinessError(BusinessException businessException) {

		        LOGGER.warn("Business exception caught", businessException);
		        ErrorResponse errorResponse = new ErrorResponse();
		        errorResponse.setErrorCode(businessException.getErrorCode());
		        errorResponse.setMessage(businessException.getMessage());
		        return new ResponseEntity<ErrorResponse>(errorResponse,
		            HttpStatus.NOT_ACCEPTABLE);
		    }

		    /**
    		 * Creates the error response.
    		 *
    		 * @param errorCode the error code
    		 * @param message the message
    		 * @return the error response
    		 */
    		private ErrorResponse createErrorResponse(String errorCode,
		                                              String message) {

		        ErrorResponse response = new ErrorResponse();
		        response.setErrorCode(errorCode);
		        response.setMessage(message);
		        return response;
		    }
}
