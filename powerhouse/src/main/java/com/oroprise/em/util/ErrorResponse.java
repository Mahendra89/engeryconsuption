package com.oroprise.em.util;

import java.io.Serializable;


/**
 * Error Response Class. This class defines generic attributes which will be sent as part of JSON
 * response in case of any Business Exceptions.
 * 
 *
 */
public class ErrorResponse
    implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 9199767617697861340L;

    /** The error code. */
    private String errorCode;

    /** The message. */
    private String message;

    /**
     * Constructor.
     */
    public ErrorResponse() {
        super();
    }

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	

}
