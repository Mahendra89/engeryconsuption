package com.oroprise.em.mapper;

import java.util.List;

import com.oroprise.em.domain.MeterDetailsJpa;
import com.oroprise.em.domain.ProfileDetailsJpa;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;

/**
 * The Interface DataMapper.
 */
public interface DataMapper {

	/**
	 * Map profile dtail to profile detail jpa.
	 *
	 * @param profileDetails
	 *            the profile details
	 * @return the list
	 */
	public List<ProfileDetailsJpa> mapProfileDtailToProfileDetailJpa(List<ProfileDetails> profileDetails);

	/**
	 * Map profile dtail jpa to profile detail.
	 *
	 * @param profileDetails
	 *            the profile details
	 * @return the list
	 */
	public List<ProfileDetails> mapProfileDtailJpaToProfileDetail(List<ProfileDetailsJpa> profileDetails);

	/**
	 * Map meter details to meter details jpa.
	 *
	 * @param meterDetails
	 *            the meter details
	 * @return the list
	 */
	public List<MeterDetailsJpa> mapMeterDetailsToMeterDetailsJpa(List<MeterDetails> meterDetails);

	/**
	 * Map meter details jpa to meter details.
	 *
	 * @param meterDetailJpas
	 *            the meter detail jpas
	 * @return the list
	 */
	public List<MeterDetails> mapMeterDetailsJpaToMeterDetails(List<MeterDetailsJpa> meterDetailJpas);

}
