package com.oroprise.em.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.oroprise.em.domain.MeterDetailsJpa;
import com.oroprise.em.domain.ProfileDetailsJpa;
import com.oroprise.em.mapper.DataMapper;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;

/**
 * The Class DataMapperImpl.
 */
@Component
public class DataMapperImpl implements DataMapper {

	/**
	 * Instantiates a new data mapper impl.
	 */
	public DataMapperImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.mapper.DataMapper#mapProfileDtailToProfileDetailJpa(java.
	 * util.List)
	 */
	@Override
	public List<ProfileDetailsJpa> mapProfileDtailToProfileDetailJpa(List<ProfileDetails> profileDetails) {
		List<ProfileDetailsJpa> profileDetailJpas = new ArrayList<>();
		for (ProfileDetails profileDetail : profileDetails) {
			ProfileDetailsJpa profileDetailJpa = new ProfileDetailsJpa();
			profileDetailJpa.setProfileName(profileDetail.getProfileName());
			profileDetailJpa.setMonth(profileDetail.getMonth());
			profileDetailJpa.setFraction(Float.parseFloat(profileDetail.getFraction()));
			profileDetailJpas.add(profileDetailJpa);
		}
		return profileDetailJpas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.mapper.DataMapper#mapProfileDtailJpaToProfileDetail(java.
	 * util.List)
	 */
	@Override
	public List<ProfileDetails> mapProfileDtailJpaToProfileDetail(List<ProfileDetailsJpa> profileDetailsJpas) {
		List<ProfileDetails> profileDetails = new ArrayList<>();
		for (ProfileDetailsJpa profileDetailJpa : profileDetailsJpas) {
			ProfileDetails profileDetail = new ProfileDetails();
			profileDetail.setProfileName(profileDetailJpa.getProfileName());
			profileDetail.setMonth(profileDetailJpa.getMonth());
			profileDetail.setFraction(String.valueOf(profileDetailJpa.getFraction()));
			profileDetails.add(profileDetail);
		}
		return profileDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.mapper.DataMapper#mapMeterDetailsToMeterDetailsJpa(java.
	 * util.List)
	 */
	@Override
	public List<MeterDetailsJpa> mapMeterDetailsToMeterDetailsJpa(List<MeterDetails> meterDetails) {
		List<MeterDetailsJpa> meterDetailsJpas = new ArrayList<>();
		for (MeterDetails meterDetail : meterDetails) {
			MeterDetailsJpa meterDetailsJpa = new MeterDetailsJpa();
			meterDetailsJpa.setMeterId(meterDetail.getMeterId());
			meterDetailsJpa.setMeterReading(Integer.parseInt(meterDetail.getMeterReading()));
			meterDetailsJpa.setMonth(meterDetail.getMonth());
			meterDetailsJpa.setProfileName(meterDetail.getProfileName());
			meterDetailsJpas.add(meterDetailsJpa);
		}
		return meterDetailsJpas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.mapper.DataMapper#mapMeterDetailsJpaToMeterDetails(java.
	 * util.List)
	 */
	@Override
	public List<MeterDetails> mapMeterDetailsJpaToMeterDetails(List<MeterDetailsJpa> meterDetailJpas) {
		List<MeterDetails> meterDetailList = new ArrayList<>();
		for (MeterDetailsJpa meterDetailJpa : meterDetailJpas) {
			MeterDetails meterDetail = new MeterDetails();
			meterDetail.setMeterId(meterDetailJpa.getMeterId());
			meterDetail.setProfileName(meterDetailJpa.getProfileName());
			meterDetail.setMonth(meterDetailJpa.getMonth());
			meterDetail.setMeterReading(String.valueOf(meterDetailJpa.getMeterReading()));
			meterDetailList.add(meterDetail);
		}
		return meterDetailList;
	}

}
