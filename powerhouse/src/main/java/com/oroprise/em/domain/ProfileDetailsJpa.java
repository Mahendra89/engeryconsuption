package com.oroprise.em.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class ProfileDetailsJpa.
 */
@Entity
@Table(name = "PROFILE_DETAIL")
public class ProfileDetailsJpa {

	/** The profile id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PROFILE_ID")
	private int profileId;

	/** The profile name. */
	@Column(name = "PROFILE_NAME")
	private String profileName;

	/** The month. */
	@Column(name = "MONTH")
	private String month;

	/** The fraction. */
	@Column(name = "FRACTIONS")
	private float fraction;

	/**
	 * Instantiates a new profile details jpa.
	 */
	public ProfileDetailsJpa() {
		super();
	}

	/**
	 * Gets the profile id.
	 *
	 * @return the profile id
	 */
	public int getProfileId() {
		return profileId;
	}

	/**
	 * Sets the profile id.
	 *
	 * @param profileId
	 *            the new profile id
	 */
	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	/**
	 * Gets the profile name.
	 *
	 * @return the profile name
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * Sets the profile name.
	 *
	 * @param profileName
	 *            the new profile name
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 *
	 * @param month
	 *            the new month
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Gets the fraction.
	 *
	 * @return the fraction
	 */
	public float getFraction() {
		return fraction;
	}

	/**
	 * Sets the fraction.
	 *
	 * @param fraction
	 *            the new fraction
	 */
	public void setFraction(float fraction) {
		this.fraction = fraction;
	}

}
