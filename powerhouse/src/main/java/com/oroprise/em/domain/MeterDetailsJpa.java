package com.oroprise.em.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class MeterDetailsJpa.
 */
@Entity
@Table(name = "METER_DETAIL")
public class MeterDetailsJpa {
	
	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PROFILE_ID")
	private int id;
	
	/** The meter id. */
	@Column(name = "METER_ID")
	private String meterId;

	/** The month. */
	@Column(name = "MONTH")
	private String month;

	/** The profile name. */
	@Column(name = "PROFILER_NAME")
	private String profileName;

	/** The meter reading. */
	@Column(name = "METER_READING")
	private int meterReading;

	/**
	 * Instantiates a new meter details jpa.
	 */
	public MeterDetailsJpa() {
		super();
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * Gets the meter id.
	 *
	 * @return the meter id
	 */
	public String getMeterId() {
		return meterId;
	}

	/**
	 * Sets the meter id.
	 *
	 * @param meterId the new meter id
	 */
	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the new month
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Gets the profile name.
	 *
	 * @return the profile name
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * Sets the profile name.
	 *
	 * @param profileName the new profile name
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * Gets the meter reading.
	 *
	 * @return the meter reading
	 */
	public int getMeterReading() {
		return meterReading;
	}

	/**
	 * Sets the meter reading.
	 *
	 * @param meterReading the new meter reading
	 */
	public void setMeterReading(int meterReading) {
		this.meterReading = meterReading;
	}

}
