package com.oroprise.em.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.oroprise.em.domain.ProfileDetailsJpa;

/**
 * The Interface ProfileDetailJpaRepository.
 */
@Repository
public interface ProfileDetailJpaRepository extends JpaRepository<ProfileDetailsJpa, Integer> {

	/**
	 * Gets the all profile name.
	 *
	 * @return the all profile name
	 */
	@Query("select distinct(profileName) from ProfileDetailsJpa")
	public List<String> getAllProfileName();
}
