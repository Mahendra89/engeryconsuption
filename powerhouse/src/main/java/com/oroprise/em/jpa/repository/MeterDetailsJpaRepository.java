package com.oroprise.em.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oroprise.em.domain.MeterDetailsJpa;

/**
 * The Interface MeterDetailsJpaRepository.
 */
@Repository
public interface MeterDetailsJpaRepository extends JpaRepository<MeterDetailsJpa, Integer> {

	/**
	 * Find consuption by meter id and month.
	 *
	 * @param meterId
	 *            the meter id
	 * @param month
	 *            the month
	 * @return the int
	 */
	@Query("Select meterReading from MeterDetailsJpa meterDeatils where meterDeatils.meterId=:meterId and meterDeatils.month=:month")
	public int findConsuptionByMeterIdAndMonth(@Param("meterId")String meterId, @Param("month")String month);

}
