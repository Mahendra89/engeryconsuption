package com.oroprise.em.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.FileUploadFileService;
import com.oroprise.em.service.MeterDetailService;
import com.oroprise.em.service.ProfileDetailService;
import com.oroprise.em.service.ValidationService;

/**
 * The Class FileUploadController.
 */
@RestController
@RequestMapping("/detail")
public class FileUploadController {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
	/** The bulk upload file service. */
	@Autowired
	FileUploadFileService bulkUploadFileService;

	/** The profile valation. */
	@Autowired
	private ValidationService<ProfileDetails> profileValation;

	/** The meter detail validation. */
	@Autowired
	private ValidationService<MeterDetails> meterDetailValidation;

	/** The profile details serv. */
	@Autowired
	private ProfileDetailService profileDetailsServ;

	/** The meter details serv. */
	@Autowired
	private MeterDetailService meterDetailsServ;

	
	/**
	 * Instantiates a new file upload controller.
	 */
	public FileUploadController() {
		super();
	}

	/**
	 * Upload profile details.
	 *
	 * @param request the request
	 * @return the response entity
	 * @throws ValidationException the validation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "/upload/profile", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
	public ResponseEntity<String> uploadProfileDetails(MultipartHttpServletRequest request)
			throws ValidationException, IOException {
		LOGGER.info("uploadProfileDetails is called");
		Iterator<String> itrator = request.getFileNames();
		MultipartFile profileDetails = request.getFile(itrator.next());
		List<ProfileDetails> profileDetailList = bulkUploadFileService.processProfileDetais(profileDetails);
		profileDetailList = profileValation.validate(profileDetailList);
		profileDetailsServ.saveProfileDetails(profileDetailList);
		return new ResponseEntity<>("Profile Details uploaded successfully", HttpStatus.OK);

	}

	/**
	 * Upload meter details.
	 *
	 * @param request the request
	 * @return the response entity
	 * @throws ValidationException the validation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "/upload/metereading", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
	public ResponseEntity<String> uploadMeterDetails(MultipartHttpServletRequest request)
			throws ValidationException, IOException {
		LOGGER.info("uploadMeterDetails is called");
		Iterator<String> itrator = request.getFileNames();
		MultipartFile meterDetails = request.getFile(itrator.next());
		List<MeterDetails> meterDetailList = bulkUploadFileService.processMeterDetails(meterDetails);
		meterDetailList = meterDetailValidation.validate(meterDetailList);
		LOGGER.info("List of valid meterDetails :{} ",meterDetailList);
		meterDetailsServ.saveMeterDetails(meterDetailList);
		return new ResponseEntity<>("Meter Details uploaded successfully", HttpStatus.OK);
	}
}
