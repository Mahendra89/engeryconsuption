package com.oroprise.em.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.EnergyConsuptionService;

/**
 * The Class MyController.
 */
@RestController
@RequestMapping("/em")
public class EnergyConsuptionController {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnergyConsuptionController.class);

	/** The energy consuption service. */
	@Autowired
	private EnergyConsuptionService energyConsuptionService;

	/**
	 * Instantiates a new my controller.
	 */
	public EnergyConsuptionController() {
		super();
	}

	/**
	 * Gets the consuption by meter id and month.
	 *
	 * @param meterId
	 *            the meter id
	 * @param month
	 *            the month
	 * @return the consuption by meter id and month
	 */
	@RequestMapping(value = "/consuption/{meterId}/{month}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Integer> getConsuptionByMeterIdAndMonth(@PathVariable String meterId,
			@PathVariable String month) {
		LOGGER.info("getConsuptionByMeterIdAndMonth is called");
		int consuption = energyConsuptionService.getConsuptionByMeterIdAndMont(meterId, month);
		LOGGER.info("Energy Constion :{}",consuption);
		return new ResponseEntity<>(consuption, HttpStatus.OK);
	}

	/**
	 * Gets the profile details.
	 *
	 * @return the profile details
	 */
	@RequestMapping(value = "/profileDetails", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<ProfileDetails>> getProfileDetails() {
		List<ProfileDetails> profileDetails = energyConsuptionService.getProfileDetails();
		return new ResponseEntity<>(profileDetails, HttpStatus.OK);
	}

	/**
	 * Gets the meter details.
	 *
	 * @return the meter details
	 */
	@RequestMapping(value = "/meterDetails", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<MeterDetails>> getMeterDetails() {
		List<MeterDetails> meterDetails = energyConsuptionService.getMeterDetails();
		return new ResponseEntity<>(meterDetails, HttpStatus.OK);
	}

}
