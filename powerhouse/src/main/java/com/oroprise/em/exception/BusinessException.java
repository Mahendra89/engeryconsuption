package com.oroprise.em.exception;

/**
 * The Class BusinessException.
 */
public class BusinessException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1373526131445357185L;

	/** The error code. */
	private String errorCode;

	/** The class name. */
	private Object className;

	/**
	 * Default Constructor calling Super.
	 * 
	 */
	public BusinessException() {
		super();
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param errorCode the error code
	 * @param className the class name
	 */
	public BusinessException(String errorCode, Object className) {
		super();
		this.errorCode = errorCode;
		this.className = className;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	public Object getClassName() {
		return className;
	}

	/**
	 * Sets the class name.
	 *
	 * @param className the new class name
	 */
	public void setClassName(Object className) {
		this.className = className;
	}

}
