package com.oroprise.em.service;

import java.util.List;

import com.oroprise.em.modal.MeterDetails;

/**
 * The Interface MeterDetailService.
 */
public interface MeterDetailService {

	/**
	 * Save meter details.
	 *
	 * @param meterDetails
	 *            the meter details
	 */
	public void saveMeterDetails(List<MeterDetails> meterDetails);
}
