package com.oroprise.em.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.jpa.repository.ProfileDetailJpaRepository;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.service.ValidationService;

/**
 * The Class MeterDetailValidationServiceImpl.
 */
@Service
public class MeterDetailValidationServiceImpl implements ValidationService<MeterDetails> {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(MeterDetailValidationServiceImpl.class);
	/** The profile detail repo. */
	@Autowired
	private ProfileDetailJpaRepository profileDetailRepo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oroprise.em.service.ValidationService#validate(java.util.List)
	 */
	@Override
	public List<MeterDetails> validate(List<MeterDetails> meterDetails) throws ValidationException {

		return validationHelper(meterDetails);
	}

	/**
	 * Validation helper.
	 *
	 * @param meterDetails
	 *            the meter details
	 * @return the list
	 */
	private List<MeterDetails> validationHelper(List<MeterDetails> meterDetails) {
		Map<String, List<MeterDetails>> groupByMeterId = transferInKeyValue(meterDetails);
		removeInvalidGroup(groupByMeterId);
		Map<String, Float> mapMeterIdsAndReadings = calculateTotalMeterReading(groupByMeterId);
		for (Map.Entry<String, List<MeterDetails>> entry : groupByMeterId.entrySet()) {
			for (int i = 0; i < entry.getValue().size() - 1; i++) {
				if (Integer.parseInt(entry.getValue().get(i).getMeterReading()) > mapMeterIdsAndReadings
						.get(entry.getKey()).intValue()) {
					LOGGER.info("Delete group from map case 3: {}",entry.getKey());
					groupByMeterId.remove(entry.getKey());
				}
			}
		}
		List<MeterDetails> finalMeterDetails = new ArrayList<>();
		for (Map.Entry<String, List<MeterDetails>> entry : groupByMeterId.entrySet()) {
			finalMeterDetails.addAll(entry.getValue());
		}
		LOGGER.info("Final list of meterDetails :{}",finalMeterDetails);
		return finalMeterDetails;
	}

	/**
	 * Removes the invalid group.
	 *
	 * @param groupByMeterId
	 *            the group by meter id
	 */
	private void removeInvalidGroup(Map<String, List<MeterDetails>> groupByMeterId) {
		List<String> profileList=getProfiles();
		for (Map.Entry<String, List<MeterDetails>> entry : groupByMeterId.entrySet()) {
			Collections.sort(entry.getValue(), MeterDetails.sortByMeterReading);
			for (int i = 0; i < entry.getValue().size() - 1; i++) {

				if (Integer.parseInt(entry.getValue().get(i).getMeterReading()) > Integer
						.parseInt(entry.getValue().get(i + 1).getMeterReading())) {
                          LOGGER.info("Delete group from map case 1: {}",entry.getKey());
					groupByMeterId.remove(entry.getKey());
					break;
				}
				if (!(profileList.contains(entry.getValue().get(i).getProfileName()))) {
					LOGGER.info("Delete group from map case 2: {}",entry.getKey());
					groupByMeterId.remove(entry.getKey());
					break;
				}

			}
		}

	}

	/**
	 * Calculate total meter reading.
	 *
	 * @param groupByMeterId
	 *            the group by meter id
	 * @return the map
	 */
	private Map<String, Float> calculateTotalMeterReading(Map<String, List<MeterDetails>> groupByMeterId) {
		Map<String, Float> groupOfMeterIdAndReading = new HashMap<>();
		for (Map.Entry<String, List<MeterDetails>> entry : groupByMeterId.entrySet()) {
			float reading = 0;
			for (int i = 0; i < entry.getValue().size() - 1; i++) {
				reading = reading + Integer.parseInt(entry.getValue().get(i).getMeterReading());

			}
			groupOfMeterIdAndReading.put(entry.getKey(), (float) (reading * (.25)));
		}
		return groupOfMeterIdAndReading;
	}

	/**
	 * Transfer in key value.
	 *
	 * @param meterDetails
	 *            the meter details
	 * @return the map
	 */
	private Map<String, List<MeterDetails>> transferInKeyValue(List<MeterDetails> meterDetails) {
		Map<String, List<MeterDetails>> groupByMeterId = new ConcurrentHashMap<>();
		List<MeterDetails> list = new ArrayList<>();
		for (MeterDetails meterdetail : meterDetails) {

			if (groupByMeterId.size() >= 0 && groupByMeterId.containsKey(meterdetail.getMeterId())) {
				list.add(meterdetail);
				groupByMeterId.put(meterdetail.getMeterId(), list);

			} else {
				list = new ArrayList<>();
				list.add(meterdetail);
				groupByMeterId.put(meterdetail.getMeterId(), list);
			}
		}
		return groupByMeterId;
	}

	/**
	 * Gets the profiles.
	 *
	 * @return the profiles
	 */
	private List<String> getProfiles() {
		List<String> profileNames = profileDetailRepo.getAllProfileName();
		LOGGER.info("List of profiles :{}",profileNames);
		return profileNames;
	}

}
