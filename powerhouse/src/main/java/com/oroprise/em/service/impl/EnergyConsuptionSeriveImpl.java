package com.oroprise.em.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oroprise.em.domain.MeterDetailsJpa;
import com.oroprise.em.domain.ProfileDetailsJpa;
import com.oroprise.em.jpa.repository.MeterDetailsJpaRepository;
import com.oroprise.em.jpa.repository.ProfileDetailJpaRepository;
import com.oroprise.em.mapper.DataMapper;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.EnergyConsuptionService;

/**
 * The Class EnergyConsuptionSeriveImpl.
 */
@Service
public class EnergyConsuptionSeriveImpl implements EnergyConsuptionService {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(EnergyConsuptionSeriveImpl.class);
	/** The meter details jpa repo. */
	@Autowired
	private MeterDetailsJpaRepository meterDetailsJpaRepo;

	/** The profile details jpa repo. */
	@Autowired
	private ProfileDetailJpaRepository profileDetailsJpaRepo;

	/** The data mapper. */
	@Autowired
	private DataMapper dataMapper;

	/**
	 * Instantiates a new energy consuption serive impl.
	 */
	public EnergyConsuptionSeriveImpl() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oroprise.em.service.EnergyConsuptionService#
	 * getConsuptionByMeterIdAndMont(java.lang.String, java.lang.String)
	 */
	@Override
	public int getConsuptionByMeterIdAndMont(String meterId, String month) {
		LOGGER.info("getConsuptionByMeterIdAndMont is called");
		
		return meterDetailsJpaRepo.findConsuptionByMeterIdAndMonth(meterId, month);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oroprise.em.service.EnergyConsuptionService#getProfileDetails()
	 */
	@Override
	public List<ProfileDetails> getProfileDetails() {
		List<ProfileDetailsJpa> profileDetailsJpas = profileDetailsJpaRepo.findAll();
		LOGGER.info("getProfileDetails is called :{}", profileDetailsJpas);
		return dataMapper.mapProfileDtailJpaToProfileDetail(profileDetailsJpas);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oroprise.em.service.EnergyConsuptionService#getMeterDetails()
	 */
	@Override
	public List<MeterDetails> getMeterDetails() {
		List<MeterDetailsJpa> meterDetailJpas = meterDetailsJpaRepo.findAll();
		LOGGER.info("getMeterDetails is called :{}", meterDetailJpas);
		return dataMapper.mapMeterDetailsJpaToMeterDetails(meterDetailJpas);
	}

}
