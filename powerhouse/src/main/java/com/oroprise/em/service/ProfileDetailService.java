package com.oroprise.em.service;

import java.util.List;

import com.oroprise.em.modal.ProfileDetails;

/**
 * The Interface ProfileDetailService.
 */
public interface ProfileDetailService {

	/**
	 * Save profile details.
	 *
	 * @param profileDeatils
	 *            the profile deatils
	 */
	public void saveProfileDetails(List<ProfileDetails> profileDeatils);

	/**
	 * Gets the all profile name.
	 *
	 * @return the all profile name
	 */
	public List<String> getAllProfileName();

}
