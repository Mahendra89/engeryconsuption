package com.oroprise.em.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;

/**
 * The Interface FileUploadFileService.
 */
public interface FileUploadFileService {

	/**
	 * Process profile detais.
	 *
	 * @param profileDetails
	 *            the profile details
	 * @return the list
	 * @throws ValidationException
	 *             the validation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<ProfileDetails> processProfileDetais(MultipartFile profileDetails)
			throws ValidationException, IOException;

	/**
	 * Process meter details.
	 *
	 * @param meterDetails
	 *            the meter details
	 * @return the list
	 * @throws ValidationException
	 *             the validation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<MeterDetails> processMeterDetails(MultipartFile meterDetails) throws ValidationException, IOException;

}
