package com.oroprise.em.service.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.FileUploadFileService;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

/**
 * The Class FileUploadServiceImpl.
 */
@Service
public class FileUploadServiceImpl implements FileUploadFileService {

	/** The Constant CSV2. */
	private static final String CSV2 = "csv";
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadServiceImpl.class);

	/**
	 * Instantiates a new file upload service impl.
	 */
	public FileUploadServiceImpl() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.service.FileUploadFileService#processProfileDetais(org.
	 * springframework.web.multipart.MultipartFile)
	 */
	@Override
	public List<ProfileDetails> processProfileDetais(MultipartFile uploadedFile)
			throws ValidationException, IOException {

		String fileExtention = FilenameUtils.getExtension(uploadedFile.getOriginalFilename());
		List<ProfileDetails> profileDetails = Collections.emptyList();
		if (!uploadedFile.isEmpty() && CSV2.equalsIgnoreCase(fileExtention)) {

			ColumnPositionMappingStrategy<ProfileDetails> strat = new ColumnPositionMappingStrategy<>();

			strat.setType(ProfileDetails.class);
			String[] columns = new String[] { "month", "profileName", "fraction" };

			strat.setColumnMapping(columns);
			CsvToBean<ProfileDetails> csv = new CsvToBean<>();

			CSVReader csvReader = new CSVReader(new InputStreamReader(uploadedFile.getInputStream()), ',', '"', 1);

			profileDetails = csv.parse(strat, csvReader);

		}
		LOGGER.info("Profile details :{}", profileDetails);
		return profileDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.service.FileUploadFileService#processMeterDetails(org.
	 * springframework.web.multipart.MultipartFile)
	 */
	@Override
	public List<MeterDetails> processMeterDetails(MultipartFile uploadedFile) throws ValidationException, IOException {
		String fileExtention = FilenameUtils.getExtension(uploadedFile.getOriginalFilename());
		List<MeterDetails> meterDetails = Collections.emptyList();
		if (!uploadedFile.isEmpty() && CSV2.equalsIgnoreCase(fileExtention)) {

			ColumnPositionMappingStrategy<MeterDetails> strat = new ColumnPositionMappingStrategy<>();

			strat.setType(MeterDetails.class);
			String[] columns = new String[] { "meterId", "profileName", "month", "meterReading" };

			strat.setColumnMapping(columns);
			CsvToBean<MeterDetails> csv = new CsvToBean<>();

			CSVReader csvReader = new CSVReader(new InputStreamReader(uploadedFile.getInputStream()), ',', '"', 1);

			meterDetails = csv.parse(strat, csvReader);

		}
		LOGGER.info("Meter details :{}", meterDetails);
		return meterDetails;
	}
}
