package com.oroprise.em.service;

import java.util.List;

import com.oroprise.em.exception.ValidationException;

/**
 * The Interface ValidationService.
 *
 * @param <T>
 *            the generic type
 */
@FunctionalInterface
public interface ValidationService<T> {

	/**
	 * Validate.
	 *
	 * @param t
	 *            the t
	 * @return the list
	 * @throws ValidationException
	 *             the validation exception
	 */
	List<T> validate(List<T> t) throws ValidationException;

}
