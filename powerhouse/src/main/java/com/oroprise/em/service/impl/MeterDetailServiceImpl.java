package com.oroprise.em.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oroprise.em.domain.MeterDetailsJpa;
import com.oroprise.em.jpa.repository.MeterDetailsJpaRepository;
import com.oroprise.em.mapper.DataMapper;
import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.service.MeterDetailService;

/**
 * The Class MeterDetailServiceImpl.
 */
@Service
public class MeterDetailServiceImpl implements MeterDetailService {

	/** The meteter details jpa repo. */
	@Autowired
	private MeterDetailsJpaRepository meteterDetailsJpaRepo;

	/** The data mapper. */
	@Autowired
	private DataMapper dataMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.service.MeterDetailService#saveMeterDetails(java.util.
	 * List)
	 */
	@Override
	public void saveMeterDetails(List<MeterDetails> meterDetails) {
		List<MeterDetailsJpa> meterDetailsJpas = dataMapper.mapMeterDetailsToMeterDetailsJpa(meterDetails);
		meteterDetailsJpaRepo.save(meterDetailsJpas);
	}

}
