package com.oroprise.em.service;

import java.util.List;

import com.oroprise.em.modal.MeterDetails;
import com.oroprise.em.modal.ProfileDetails;

/**
 * The Interface EnergyConsuptionService.
 */
public interface EnergyConsuptionService {

	/**
	 * Gets the consuption by meter id and mont.
	 *
	 * @param meterId
	 *            the meter id
	 * @param month
	 *            the month
	 * @return the consuption by meter id and mont
	 */
	public int getConsuptionByMeterIdAndMont(String meterId, String month);

	/**
	 * Gets the profile details.
	 *
	 * @return the profile details
	 */
	public List<ProfileDetails> getProfileDetails();

	/**
	 * Gets the meter details.
	 *
	 * @return the meter details
	 */
	public List<MeterDetails> getMeterDetails();
}
