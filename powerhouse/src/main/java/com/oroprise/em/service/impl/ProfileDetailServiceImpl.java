package com.oroprise.em.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oroprise.em.domain.ProfileDetailsJpa;
import com.oroprise.em.jpa.repository.ProfileDetailJpaRepository;
import com.oroprise.em.mapper.DataMapper;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.ProfileDetailService;

/**
 * The Class ProfileDetailServiceImpl.
 */
@Service
public class ProfileDetailServiceImpl implements ProfileDetailService {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileDetailServiceImpl.class);
	/** The profile detail repo. */
	@Autowired
	private ProfileDetailJpaRepository profileDetailRepo;

	/** The data mapper. */
	@Autowired
	private DataMapper dataMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.oroprise.em.service.ProfileDetailService#saveProfileDetails(java.util
	 * .List)
	 */
	@Override
	public void saveProfileDetails(List<ProfileDetails> profileDeatails) {
		List<ProfileDetailsJpa> profileDetailJpas = dataMapper.mapProfileDtailToProfileDetailJpa(profileDeatails);
		LOGGER.info("saveProfileDetails is called :{}",profileDeatails);
		profileDetailRepo.save(profileDetailJpas);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oroprise.em.service.ProfileDetailService#getAllProfileName()
	 */
	@Override
	public List<String> getAllProfileName() {
	
		return profileDetailRepo.getAllProfileName();
	}

}
