package com.oroprise.em.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.oroprise.em.exception.ValidationException;
import com.oroprise.em.modal.ProfileDetails;
import com.oroprise.em.service.ValidationService;

/**
 * The Class ProfileDetailsValidationServiceImpl.
 */
@Service
public class ProfileDetailsValidationServiceImpl implements ValidationService<ProfileDetails> {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileDetailServiceImpl.class);
	
	/**
	 * Instantiates a new profile details validation service impl.
	 */
	public ProfileDetailsValidationServiceImpl() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.oroprise.em.service.ValidationService#validate(java.util.List)
	 */
	@Override
	public List<ProfileDetails> validate(List<ProfileDetails> profileDetails) throws ValidationException {
		
		Map<String, Float> profileFractionMaps = validationHelper(profileDetails);
		for (Map.Entry<String, Float> entry : profileFractionMaps.entrySet()) {
			if (1 != entry.getValue().intValue()) {
				throw new ValidationException();
			}
		}
		LOGGER.info("Valid profileDetails :{}",profileDetails);
		return profileDetails;
	}

	/**
	 * Validation helper.
	 *
	 * @param profiles
	 *            the profiles
	 * @return the map
	 */
	private Map<String, Float> validationHelper(List<ProfileDetails> profiles) {
		Map<String, Float> maps = new HashMap<>();
		for (ProfileDetails profile : profiles) {
			if (maps.get(profile.getProfileName()) != null) {
				float f1 = Float.valueOf(profile.getFraction());
				float f2 = Float.valueOf(maps.get(profile.getProfileName()));

				float fraction = f1 + f2;
				maps.put(profile.getProfileName(), fraction);
			} else {
				float fraction = Float.valueOf(profile.getFraction());
				maps.put(profile.getProfileName(), fraction);
			}
		}

		return maps;
	}


}
