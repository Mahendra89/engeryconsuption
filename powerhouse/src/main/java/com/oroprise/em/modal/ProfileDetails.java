package com.oroprise.em.modal;

public class ProfileDetails {
	private String profileName;
	private String month;
	private String fraction;
	
	
	public ProfileDetails() {
		super();
	}
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getFraction() {
		return fraction;
	}
	public void setFraction(String fraction) {
		this.fraction = fraction;
	}
	@Override
	public String toString() {
		return "ProfileDetail [profileName=" + profileName + ", month=" + month + ", fraction=" + fraction + "]";
	}
	

}
