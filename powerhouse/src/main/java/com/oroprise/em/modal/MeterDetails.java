package com.oroprise.em.modal;

import java.util.Comparator;

public class MeterDetails {
	
public MeterDetails() {
		super();
	}
private String meterId;
private String profileName;
private String month;
private  String meterReading;
public static  Comparator<MeterDetails> sortByMeterReading=new Comparator<MeterDetails>() {

	@Override
	public int compare(MeterDetails o1, MeterDetails o2) {
		return Integer.parseInt(o1.getMeterReading())-Integer.parseInt(o2.getMeterReading());
	}
};

public String getMeterId() {
	return meterId;
}
public void setMeterId(String meterId) {
	this.meterId = meterId;
}
public String getProfileName() {
	return profileName;
}
public void setProfileName(String profileName) {
	this.profileName = profileName;
}
public String getMonth() {
	return month;
}
public void setMonth(String month) {
	this.month = month;
}
public String getMeterReading() {
	return meterReading;
}
public void setMeterReading(String meterReading) {
	this.meterReading = meterReading;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((meterId == null) ? 0 : meterId.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	MeterDetails other = (MeterDetails) obj;
	if (meterId == null) {
		if (other.meterId != null)
			return false;
	} else if (!meterId.equals(other.meterId))
		return false;
	return true;
}
@Override
public String toString() {
	return "MeterDetails [meterId=" + meterId + ", profileName=" + profileName + ", month=" + month + ", meterReading="
			+ meterReading + "]";
}



}
